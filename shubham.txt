Drill_1:
1.
To create the root "hello" directory : mkdir hello
To move inside "hello" : cd hello
To make "five" inside "hello" : mkdir five
To make "six" inside "hello/five" : mkdir six
To create "c.txt" inside "hello/five/six" : touch c.txt
To make "seven" inside "hello/five/six" : mkdir seven
To create "error.log" inside "hello/five/six/seven" : touch error.log
To go back to the root and make "One" : cd ..(till we reach "hello")
Now we are at "hello"
To make "One" inside "hello" : mkdir one
To create "a.txt" inside "hello/one" : touch a.txt
To create "b.txt" inside "hello/one" : touch b.txt
To make "two" inside "hello/two" : mkdir two
To create "d.txt" inside "hello/one/two" : touch d.txt
To make "three" inside "hello/two/three" : mkdir three
To create "e.txt" inside "hello/one/two/three" : touch e.txt
To make "four" inside "hello/two/three" : mkdir four
To create "access.log" inside "hello/one/two/three/four" : touch access.log

2.
To delete all log files : find -name *.log -delete

3.
To add content to a.txt echo "content" > a.txt

4.
To delete directory named five : rm -r five

DRILL_2

1. To Download text file : wget url-link
2. To print first 3 lines of the text file: head -3 harry-potter.txt
3. To print last 10 lines of text file: tail -10 harry-potter.txt
4. To count occurences of "Harry" in the file : $ grep -o 'Harry' harry-potter.txt | wc -l
5. To count occurences of "Ron" in the file : $ grep -o 'Ron' harry-potter.txt | wc -l
6. To count occurences of "Hermione" in the file : $ grep -o 'Hermione' harry-potter.txt | wc -l
7. To count occurences of "Dumbledore" in the file : $ grep -o 'Dumbledore' harry-potter.txt | wc -l
8. To print lines from 100 to 200 : sed -n '100,+99p' harry-potter.txt
9. To find no of unique words in file : awk '{for(i = 1; i <= NF; i++) {a[$i]++}} END {for(k in a) if(a[k] == 1){counter++} print counter}' harry-potter.txt

Process:
To get browser's process id: pgrep firefox
To get parent process id : ps -o ppid,pid <process,id>
To stop browser : pkill firefox or kill pid of firefox
To get top 3 process by cpu usage : ps -eo comm,%cpu --sort=-%cpu | head -4

Misc:
To get google's address : install syslinux-utils then type gethostip -d google.com
To get local ip address : hostname --ip-address or install net-tools and type if config.
